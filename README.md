This Java project is a Maven based WEB UI test project to test dotomvc.com/examples/angularjs single page application

selenium is used for WebDriver related functionality

Page Object Model approach is used to capture and create libraries of web pages

TestNG is used as the test driving framework

Tests can be executed individually, (from within an IDE) or can be run from command line (mvn test –Dbrowser=”chrome”)
(ONLY chrome / firefox browsers are supported)
Browser can be either set in POM or can be provided as a command line parm,

Test data is provided by @DataProviders (for simplicity), setTodo is repeated in all the tests to make each test self-content, (independent)



For simplicity ONLY chrome / firefox browsers are supported from Mac/linux/unix
(driver for the above is obtained from DriverFactory class, similar approach is required for Windows OS and other browsers (ex IE, edge, safari ..etc)

DriverFactory class is very basic, this should be enhanced to provide “remotedrivers” which is required for running tests in parallel using selenium grid, (as of now, this projects doesn’t support remotedrivers)

Generic Exception is used, which should be enhanced with Domain specific exceptions

Mostly positive test scenarios are covered, (negative flows, error handling , field level validations are NOT covered)

EditTodo item is not covered as Selenium doesn’t directly provide the ability to update Label elements,
Should have to use Javascript executor provided by selenium to update the innerText of the Label to change it’s content, a bit challenging,
