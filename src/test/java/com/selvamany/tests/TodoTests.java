package com.selvamany.tests;

import com.selvamany.common.DriverFactory;
import com.selvamany.pageobjects.AngularJSPage;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.*;


/**
 * Todo List Tests
 */
public class TodoTests {

    private WebDriver driver;
    private AngularJSPage angularJSPage;

    @BeforeMethod
    public void setup()throws Exception{
        driver=DriverFactory.initialize();
        angularJSPage = new AngularJSPage(driver);
        driver.get("http://todomvc.com");
        angularJSPage.accessAngularPage();
    }

    @Test( testName="addTodoTest",dataProvider = "addTodoTestData",enabled = true)
    public void addTodoTest(String[] todoItems) throws Exception{
        angularJSPage.setTodo(todoItems);
        Assert.assertEquals(todoItems, angularJSPage.getTodoList().toArray());
    }

    @Test( testName="completeTodoTest",dataProvider = "completeTodoTestData",enabled = true)
    public void completeTodoTest(String[] todoItems, String todoToComplete) throws Exception {
        angularJSPage.setTodo(todoItems);
        angularJSPage.completeTodoItem(todoToComplete);
        Assert.assertEquals(angularJSPage.getTodoItemStatus(todoToComplete),"completed","Todo completed test Failed, Todo NOT marked as Completed ....");
    }

    @Test( testName="removeTodoTest",dataProvider = "removeTodoTestData",enabled = true)
    public void removeTodoTest(String[] todoItems, String todoToRemove) throws Exception {
        angularJSPage.setTodo(todoItems);
        angularJSPage.removeTodoItem(todoToRemove);
        Assert.assertFalse(angularJSPage.getTodoList().contains(todoToRemove), "todoRemove failed..todo List contains the removed item!..");
    }

    @Test( testName="completedFilterTest",dataProvider = "completedFilterTestData",enabled = true)
    public void completedFilterTest(String[] todoItems, String[] todoTocomplete) throws Exception {
        angularJSPage.setTodo(todoItems);
        for(String item: todoTocomplete){
            angularJSPage.completeTodoItem(item);
        }
        angularJSPage.setCompletedFilter();
        Assert.assertEquals(angularJSPage.getTodoList().toArray(), todoTocomplete, "Completed Filter test Failed...");
    }

    @Test( testName="clearCompletedTodoTest",dataProvider = "completedFilterTestData",enabled = true)
    public void clearCompletedTodoTest(String[] todoItems, String[] todoTocomplete) throws Exception {
        angularJSPage.setTodo(todoItems);
        for (String item : todoTocomplete) {
            angularJSPage.completeTodoItem(item);
        }
        angularJSPage.clearCompletedTodos();
        Assert.assertFalse(angularJSPage.getTodoList().contains(todoTocomplete), "Clear Completed todo failed..todo List contains the removed item!..");
    }

    @AfterMethod
    public void tearDown(){
        driver.quit();
    }


    @DataProvider(name = "addTodoTestData")
    public static Object[][] addTodoTestDataProvider() {
        return new Object[][]{
                {new String[]{"FirstTodo","SecondTodo","ThirdTodo","FourthTodo"}}
        };

    }

    @DataProvider(name = "completeTodoTestData")
    public static Object[][] completeTodoTestDataProvider() {
        return new Object[][]{
                {new String[]{"FirstTodo","SecondTodo","ThirdTodo","FourthTodo"},"FourthTodo"}
        };

    }

    @DataProvider(name = "removeTodoTestData")
    public static Object[][] removeTodoTestDataProvider() {
        return new Object[][]{
                {new String[]{"FirstTodo","SecondTodo","ThirdTodo","FourthTodo"},"FirstTodo"}
        };

    }

    @DataProvider(name = "completedFilterTestData")
    public static Object[][] completedFilterTestDataProvider() {
        return new Object[][]{
                {new String[]{"FirstTodo","SecondTodo","ThirdTodo","FourthTodo"},new String[]{"SecondTodo","FourthTodo"}}
        };

    }
}
