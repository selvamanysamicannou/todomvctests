package com.selvamany.common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Basic DriverFactory to obtain driver instance
 */
public class DriverFactory {


    private static WebDriver driver;
    private static String browserFromProperty;
    private static String OS;


    public static WebDriver initialize() throws Exception{

       browserFromProperty = System.getProperty("browser");
        OS=System.getProperty("os.name").toLowerCase();

        switch(browserFromProperty){
            case "chrome":
            case "CHROME":
            {
                if(OS.contains("mac")) {
                    System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver-mac");
                }else if(OS.contains("unix") || OS.contains("linux")){
                    System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver-linux");
                }else{
                    throw new Exception("... OS type NOT Supported for Browse Driver ....");
                }
                driver = new ChromeDriver();
                break;
            }
            case "FireFox":
            case "firefox":
            {
                if(OS.contains("mac")) {
                    System.setProperty("webdriver.gecko.driver", "src/test/resources/geckodriver-mac");
                }else if(OS.contains("unix") || OS.contains("linux")){
                    System.setProperty("webdriver.gecko.driver", "src/test/resources/geckodriver-linux");
                }else{
                    throw new Exception("... OS type NOT Supported for Browse Driver ....");
                }
                driver = new FirefoxDriver();
                break;
            }
            default :
             throw new Exception("...Browser Type Not Supported, ONLY Chrome & Firefox are supported ....!");

        }
        return driver;

    }
}
