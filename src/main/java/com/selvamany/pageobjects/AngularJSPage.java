package com.selvamany.pageobjects;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.paulhammant.ngwebdriver.ByAngular;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

/**
 * Page Object of the AngularJS page that contains the elements and functions around them
 */
public class AngularJSPage {

    @FindBy(linkText ="AngularJS" )
    WebElement angularJSLink;

    @FindBy(id ="new-todo" )
    WebElement todoInput;

    @FindBy(id ="clear-completed")
    WebElement clearCompletedButton;

    @FindBy(linkText ="Completed" )
    WebElement completedFilter;

    private WebElement todoItemWebElement;
    private WebDriver driver;
    private Actions action;
    private Wait wait;
    JavascriptExecutor js;

    public AngularJSPage(WebDriver driver1){
        PageFactory.initElements(driver1, this);
        this.driver = driver1;
        action = new Actions(driver);
        js = (JavascriptExecutor) driver;
    }

    public AngularJSPage accessAngularPage(){
        angularJSLink.click();
        return this;
    }

    public AngularJSPage setTodo(String todoItem)throws Exception{
        wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("new-todo")));
        todoInput.sendKeys(todoItem);
        todoInput.sendKeys(Keys.ENTER);
        return this;
    }

    public AngularJSPage setTodo(String ... todoItems)throws Exception{
        for(String item : todoItems){
            setTodo(item);
        }
        return this;
    }

    public List<String> getTodoList()throws Exception{
        List<String> todoList = new ArrayList<>();
        List<WebElement> todoListWebElements;
        try {
            todoListWebElements = driver.findElements(ByAngular.repeater("todo in todos"));
            for (WebElement todo : todoListWebElements) {
                todoList.add(todo.getText());
            }
        }catch(NoSuchElementException e){
            throw new Exception("...ToDo List is empty....!");
        }
        return todoList;
    }

    public List<WebElement> getTodoListWebElements()throws Exception{
        List<WebElement> todoListWebElements;
        try{
            todoListWebElements = driver.findElements(ByAngular.repeater("todo in todos"));
        }catch(NoSuchElementException e){
            throw new Exception("...ToDo List is empty....!");
        }
        return todoListWebElements;
    }

    public WebElement accessSpecificIteminTodoList(String todoItem)throws Exception{
        Boolean itemFound=false;
        List<WebElement> todoListElements = getTodoListWebElements();
        if(todoListElements.size()>0) {
            for (WebElement todo : todoListElements) {
                if (todo.getText().equalsIgnoreCase(todoItem)) {
                    itemFound = true;
                    todoItemWebElement=todo;
                    break;
                }
            }
        } else{
            throw new Exception("TODO List is Empty!..");
        }

        if(!itemFound){
            throw new Exception("TODO item not found in the List !..."+todoItem);
        }
        return todoItemWebElement;
    }


    public void completeTodoItem(String item)throws Exception{
        WebElement todoElement = accessSpecificIteminTodoList(item);
        WebElement todoComplete = todoElement.findElement(ByAngular.model("todo.completed"));
        todoComplete.click();
    }

    public void setCompletedFilter(){
        completedFilter.click();
    }

    public void clearCompletedTodos(){
        clearCompletedButton.click();

    }

    public void removeTodoItem(String item)throws Exception{
        WebElement todoElement = accessSpecificIteminTodoList(item);
        action = new Actions(driver);
        action.moveToElement(todoElement).perform();
        WebElement todoRemove = todoElement.findElement(By.xpath(".//following-sibling::button[@class='destroy']"));
        todoRemove.click();
    }

    public String getTodoItemStatus(String todoItem)throws Exception{
        String status ="";
        WebElement todoElement = accessSpecificIteminTodoList(todoItem);
        WebElement todoLabel = todoElement.findElement(By.xpath(".//div/label"));
        String itemTextType=todoLabel.getCssValue("text-decoration");
        if(todoLabel.getCssValue("text-decoration").contains("line-through")){
            return status="completed";
        }else{
            return status="active";
        }
    }


}
